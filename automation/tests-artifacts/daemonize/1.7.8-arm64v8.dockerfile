FROM arm64v8/alpine:3.12
LABEL maintainer.name="The runX Project" \
      maintainer.email="eve-runx@lists.lfedge.org"

ENV USER root

RUN mkdir /build
WORKDIR /build

# build depends
RUN apk update && \
    \
    # runx
    apk add autoconf && \
    apk add automake && \
    apk add bash && \
    apk add curl && \
    apk add gcc  && \
    apk add g++ && \
    apk add git && \
    apk add make && \
    \
    # Build daemonize
    git clone https://github.com/bmc/daemonize.git && \
    cd daemonize && \
    git checkout release-1.7.8 && \
    sh configure && \
    make && \
    cp daemonize / && \
    cd /build && \
    rm -rf daemonize && \
    rm -rf /tmp/* && \
    rm -f /var/cache/apk/*

