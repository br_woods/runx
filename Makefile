
ifeq ($(CC),)
GCC_VERSION=$(shell gcc -dumpversion)
ifeq ($(GCC_VERSION),10)
$(error bad gcc version, 10 is not compatable with the kernel used)
endif
endif


.PHONY: all
all:
	./build.sh

.PHONY: clean
clean:
	./build.sh clean
